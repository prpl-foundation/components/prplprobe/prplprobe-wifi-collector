/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <gtest/gtest.h>

#include <prplprobe/common.pb.h>
#include <prplprobe/wifi_interface_stats.pb.h>

#include "mock/mock_bus.h"

using namespace prplprobe::internal::v1;
using namespace kpi::wifi::v1;

extern void getInterfaceStats(EventList *event_list, string object_name);

void populate_datamodel() {
    amxd_dm_t* dm = mock_get_dm();

    amxd_object_t *wifi_obj = add_object_datamodel(amxd_dm_get_root(dm), "WiFi");
    amxd_object_t *wifi_radio_obj = add_template_datamodel(wifi_obj, "Radio");
    amxd_object_t *wifi_radio_wifi0_obj = add_instance_datamodel(wifi_radio_obj, "wifi0", 1);

    add_parameter_datamodel(wifi_radio_wifi0_obj, "OperatingFrequencyBand", AMXC_VAR_ID_CSTRING);
    amxd_object_set_cstring_t(wifi_radio_wifi0_obj, "OperatingFrequencyBand", "2.4GHz");

    amxd_object_t *wifi_radio_wifi0_stats_obj = add_object_datamodel(wifi_radio_wifi0_obj, "Stats");

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "BytesSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "BytesSent", 1);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "BytesReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "BytesReceived", 2);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "PacketsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "PacketsSent", 3);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "PacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "PacketsReceived", 4);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "ErrorsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "ErrorsSent", 5);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "ErrorsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "ErrorsReceived", 6);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "DiscardPacketsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "DiscardPacketsSent", 7);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "DiscardPacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "DiscardPacketsReceived", 8);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "UnicastPacketsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "UnicastPacketsSent", 9);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "UnicastPacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "UnicastPacketsReceived", 10);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "MulticastPacketsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "MulticastPacketsSent", 11);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "MulticastPacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "MulticastPacketsReceived", 12);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "BroadcastPacketsSent", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "BroadcastPacketsSent", 13);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "BroadcastPacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "BroadcastPacketsReceived", 14);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "UnknownProtoPacketsReceived", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "UnknownProtoPacketsReceived", 15);

    add_parameter_datamodel(wifi_radio_wifi0_stats_obj, "RetransCount", AMXC_VAR_ID_UINT64);
    amxd_object_set_uint64_t(wifi_radio_wifi0_stats_obj, "RetransCount", 16);

    amxd_object_t *wifi_radio_wifi0_stats_wmmfailedsent_obj = add_object_datamodel(wifi_radio_wifi0_stats_obj, "WmmFailedSent");

    add_parameter_datamodel(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_BE", AMXC_VAR_ID_INT32);
    amxd_object_set_int32_t(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_BE", 17);

    add_parameter_datamodel(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_BK", AMXC_VAR_ID_INT32);
    amxd_object_set_int32_t(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_BK", 18);

    add_parameter_datamodel(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_VO", AMXC_VAR_ID_INT32);
    amxd_object_set_int32_t(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_VO", 19);

    add_parameter_datamodel(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_VI", AMXC_VAR_ID_INT32);
    amxd_object_set_int32_t(wifi_radio_wifi0_stats_wmmfailedsent_obj, "AC_VI", 20);
}

// Basic test to check googletest works correctly
TEST(wifi, getInterfaceStats) {
    setup_amx_bus();
    populate_datamodel();

    EventList event_list;
    getInterfaceStats(&event_list, "Radio");
    EXPECT_EQ(event_list.event_size(), 1);

    Event event = event_list.event(0);
    EXPECT_EQ(event.kpiname(), "Wifi");
    EXPECT_EQ(event.kpitype(), "InterfaceStats");

    WifiInterfaceStats intfStats;
    event.event_data().UnpackTo(&intfStats);

    EXPECT_EQ(intfStats.bytessent(), 1);
    EXPECT_EQ(intfStats.bytesreceived(), 2);
    EXPECT_EQ(intfStats.packetssent(), 3);
    EXPECT_EQ(intfStats.packetsreceived(), 4);
    EXPECT_EQ(intfStats.txerror(), 5);
    EXPECT_EQ(intfStats.rxerror(), 6);
    EXPECT_EQ(intfStats.txnobuf(), 7);
    EXPECT_EQ(intfStats.discardpacketsreceived(), 8);
    EXPECT_EQ(intfStats.unicastpacketssent(), 9);
    EXPECT_EQ(intfStats.unicastpacketsreceived(), 10);
    EXPECT_EQ(intfStats.multicastpacketssent(), 11);
    EXPECT_EQ(intfStats.multicastpacketsreceived(), 12);
    EXPECT_EQ(intfStats.broadcastpacketssent(), 13);
    EXPECT_EQ(intfStats.broadcastpacketsreceived(), 14);
    EXPECT_EQ(intfStats.unknownprotopacketsreceived(), 15);
    EXPECT_EQ(intfStats.txretrans(), 16);
    EXPECT_EQ(intfStats.wmmfailedsent(0), 17);
    EXPECT_EQ(intfStats.wmmfailedsent(1), 18);
    EXPECT_EQ(intfStats.wmmfailedsent(2), 19);
    EXPECT_EQ(intfStats.wmmfailedsent(3), 20);

    cleanup_amx_bus();
}
