# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.0.15 - 2024-01-23(17:10:49 +0000)

### Other

- - [Prpl] WiFi Scan event must have exactly the same format than on SOP

## Release v0.0.14 - 2023-12-18(13:03:13 +0000)

## Release v0.0.13 - 2023-11-02(07:15:21 +0000)

### Other

- - [Eyes'ON][prpl] Document Eyes'ON Prpl APIs

## Release v0.0.12 - 2023-10-20(12:44:21 +0000)

### Other

- - Probe Crash

## Release v0.0.11 - 2023-10-17(07:19:29 +0000)

### Other

- - [Prpl] Compile containers with ??? version of LCM SDK for Network X

## Release v0.0.10 - 2023-10-06(12:05:57 +0000)

### Other

- Opensource component

## Release v0.0.9 - 2023-09-19(08:44:55 +0000)

### Other

- - [Eyes'ON][prpl] Add CI tests on new Eyes'ON prpl components
- - [Prpl] Rename all "agent" into "prplprobe agent"

## Release v0.0.8 - 2023-08-28(09:12:55 +0000)

### Other

- - [Eyes'ON][prpl] Enable core dump save

## Release v0.0.7 - 2023-08-25(12:58:32 +0000)

### Other

- - [Eyes'ON][prpl] Add a client lib to have common code and simplify integration of new modules
- - [Eyes'ON][prpl] Make SAH CI work on agent and modules
- - [Prpl] Package for open source delivery

## Release v0.0.6 - 2023-05-02(18:20:45 +0000)

### Other

- - gRPC clean integration

## Release v0.0.5 - 2023-04-07(09:24:55 +0000)

### Other

- - [Eyes'ON][prpl] Integrate Eyes'ON agent + modules in LCM

## Release v0.0.4 - 2023-03-10(09:38:04 +0000)

### Other

- - Use only libsahtrace for logging messages

## Release v0.0.3 - 2023-02-23(14:16:07 +0000)

### Other

- - [eyeson] Crash of wifi-module and deviceinfo-module at boot (including first boot)

## Release v0.0.2 - 2023-01-25(12:54:33 +0000)

### Other

- - Update protobuf schemas packages to fit organization rules

## Release v0.0.1 - 2022-12-09(15:33:06 +0000)

### Other

- - [Prpl] Clean and merge all devs that are in branch

