#!/bin/sh

[ -f /etc/environment ] && source /etc/environment

ulimit -c ${ULIMIT_CONFIGURATION:-0}

start() {
    /usr/lib/prplprobe/collectors/prplprobe-wifi-collector
}

stop() {
    kill $(pidof prplprobe-wifi-collector)
}

case $1 in
    start|boot)
        start
        ;;
    stop|shutdown)
        stop
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        echo "TODO debuginfo prplprobe-wifi-collector"
        ;;
    log)
        echo "TODO log prplprobe-wifi-collector"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo|log]"
        ;;
esac
