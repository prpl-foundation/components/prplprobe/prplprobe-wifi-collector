/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <regex>
#include <string>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <amxrt/amxrt.h>

#include <prplprobe/common.pb.h>
#include <prplprobe/module.pb.h>
#include <prplprobe/wifi_interface_stats.pb.h>
#include <prplprobe/wifi_radio.pb.h>
#include <prplprobe/wifi_radio_air_stats.pb.h>
#include <prplprobe/wifi_station_stats.pb.h>
#include <prplprobe/wifi_scan.pb.h>
#include <prplprobe/wifi_scan_count.pb.h>

#include <prplprobe/amx_client_utils.h>
#include <prplprobe/amx_utils.h>
#include <prplprobe/config_utils.h>
#include <prplprobe/event_loop.h>
#include <prplprobe/func_utils.h>
#include <prplprobe/grpc_utils.h>
#include <prplprobe/sahtrace_utils.h>

#include "wifi.h"

#define ME "wifi"
#define SOURCE "Wifi"

#define GET_UINT8(a, n) amxc_var_dyncast(uint8_t, n == NULL ? a : GET_ARG(a, n))
#define GET_UINT64(a, n) amxc_var_dyncast(uint64_t, n == NULL ? a : GET_ARG(a, n))
#define GET_CSV_STRING(a, n) amxc_var_constcast(csv_string_t, n == NULL ? a : GET_ARG(a, n))

using namespace std;
using namespace prplprobe::internal::v1;
using namespace kpi::wifi::v1;
using namespace google::protobuf;

ModuleConfiguration wifi_configuration;

/* Common functions */

string getRadioShortname(string freq) {
    if (freq == "2.4GHz") {
        return "wifi0";
    }
    else if (freq == "5GHz") {
        return "wifi1";
    }
    else if (freq == "6GHz") {
        return "wifi2";
    }
    else {
        return "Undefined";
    }
}

string getSSIDFreq(amxc_var_t *ssid) {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
    }
    amxc_var_t reply;
    amxc_var_init(&reply);
    string path = string(GET_CHAR(ssid, "LowerLayers") ? : "") + ".OperatingFrequencyBand";
    int err = amxb_get(bus_ctx, path.c_str(), 0, &reply, 5);
    if (err != 0) {
        SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
    }
    string freq = GET_CHAR(amxc_var_get_first(amxc_var_get_first(&reply)), "OperatingFrequencyBand") ? : "";
    amxc_var_clean(&reply);
    return freq;
}

string getFreqFromRadioRef(string radio_ref) {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
    }
    amxc_var_t reply;
    amxc_var_init(&reply);
    string path = radio_ref + ".OperatingFrequencyBand";
    int err = amxb_get(bus_ctx, path.c_str(), 0, &reply, 5);
    if (err != 0) {
        SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
    }
    string freq = GET_CHAR(amxc_var_get_first(amxc_var_get_first(&reply)), "OperatingFrequencyBand") ? : "";
    amxc_var_clean(&reply);
    return freq;
}

/* END */

/* InterfaceStats */

void getInterfaceStats(EventList *event_list, string object_name) {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return;
    }

    string path = "WiFi." + object_name + ".";
    amxb_request_t *req = amxb_async_call(bus_ctx, path.c_str(), "_get", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return;
    }

    amxc_var_for_each(intf, amxc_var_get_first(req->result)) {

        if (amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, intf))) {
            continue;
        }

        WifiInterfaceStats intfStats;
        string intf_path = amxc_var_key(intf);
        if (object_name == "Radio") {
            regex pattern("WiFi.Radio.\\d+.");
            if (!regex_match(intf_path, pattern)) {
                continue;
            }
            string freq = GET_CHAR(intf, "OperatingFrequencyBand") ? : "";
            intfStats.set_interface(getRadioShortname(freq));
            intfStats.set_flag("Radio");
        }
        else if (object_name == "SSID") {
            regex pattern("WiFi.SSID.\\d+.");
            if (!regex_match(intf_path, pattern)) {
                continue;
            }
            intfStats.set_interface(getRadioShortname(getSSIDFreq(intf)));
            /* Can't find VAPMode */
            intfStats.set_vapmode("Undefined");
            intfStats.set_hotspottype("Undefined");
            /* END */
            intfStats.set_flag("VAP");
        }

        amxc_var_t ret;
        amxc_var_init(&ret);
        string stats_path = amxc_var_key(intf);
        stats_path.append("Stats.");
        int err = amxb_get(bus_ctx, stats_path.c_str(), 0, &ret, 5);
        if (err != 0) {
            SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
        }

        amxc_var_t *stats = NULL;
        amxc_var_for_each(item, amxc_var_get_first(&ret)) {
            string key = amxc_var_key(item);
            if (stats_path == key) {
                stats = item;
                break;
            }
        }
        if (!stats) {
            amxc_var_clean(&ret);
            continue;
        }

        intfStats.set_bytessent(GET_UINT64(stats, "BytesSent"));
        intfStats.set_bytesreceived(GET_UINT64(stats, "BytesReceived"));
        intfStats.set_packetssent(GET_UINT64(stats, "PacketsSent"));
        intfStats.set_packetsreceived(GET_UINT64(stats, "PacketsReceived"));
        intfStats.set_txerror(GET_UINT64(stats, "ErrorsSent"));
        intfStats.set_rxerror(GET_UINT64(stats, "ErrorsReceived"));
        intfStats.set_txnobuf(GET_UINT64(stats, "DiscardPacketsSent"));
        intfStats.set_discardpacketsreceived(GET_UINT64(stats, "DiscardPacketsReceived"));
        intfStats.set_unicastpacketssent(GET_UINT64(stats, "UnicastPacketsSent"));
        intfStats.set_unicastpacketsreceived(GET_UINT64(stats, "UnicastPacketsReceived"));
        intfStats.set_multicastpacketssent(GET_UINT64(stats, "MulticastPacketsSent"));
        intfStats.set_multicastpacketsreceived(GET_UINT64(stats, "MulticastPacketsReceived"));
        intfStats.set_broadcastpacketssent(GET_UINT64(stats, "BroadcastPacketsSent"));
        intfStats.set_broadcastpacketsreceived(GET_UINT64(stats, "BroadcastPacketsReceived"));
        intfStats.set_unknownprotopacketsreceived(GET_UINT64(stats, "UnknownProtoPacketsReceived"));
        intfStats.set_txretrans(GET_UINT64(stats, "RetransCount"));

        amxc_var_t ret2;
        amxc_var_init(&ret2);
        string wwmfailed_path = amxc_var_key(stats);
        wwmfailed_path.append("WmmFailedSent.");
        err = amxb_get(bus_ctx, wwmfailed_path.c_str(), 1, &ret2, 5);
        if (err != 0) {
            SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
        }

        amxc_var_t *wmmfailedsent = NULL;
        amxc_var_for_each(item, amxc_var_get_first(&ret2)) {
            string key = amxc_var_key(item);
            if (wwmfailed_path == key) {
                wmmfailedsent = item;
                break;
            }
        }
        if (!wmmfailedsent) {
            amxc_var_clean(&ret);
            amxc_var_clean(&ret2);
            continue;
        }

        intfStats.add_wmmfailedsent(GET_INT32(wmmfailedsent, "AC_BE"));
        intfStats.add_wmmfailedsent(GET_INT32(wmmfailedsent, "AC_BK"));
        intfStats.add_wmmfailedsent(GET_INT32(wmmfailedsent, "AC_VO"));
        intfStats.add_wmmfailedsent(GET_INT32(wmmfailedsent, "AC_VI"));

        Event *event = event_list->add_event();
        event->set_kpiname(SOURCE);
        event->set_kpitype("InterfaceStats");
        fill_timestamp(event);
        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(intfStats);

        amxc_var_clean(&ret);
        amxc_var_clean(&ret2);
    }

    amxb_close_request(&req);
}

EventList *getInterfaceStatsEventList() {
    EventList *event_list = new EventList();

    getInterfaceStats(event_list, "Radio");
    getInterfaceStats(event_list, "SSID");

    return event_list;
}

/* END InterfaceStats */

/* Radio */

EventList *getRadioEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return NULL;
    }

    amxb_request_t *req = amxb_async_call(bus_ctx, "WiFi.Radio.", "_get", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return NULL;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return NULL;
    }

    EventList *event_list = new EventList();

    amxc_var_for_each(intf, amxc_var_get_first(req->result)) {

        if (amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, intf))) {
            continue;
        }

        string intf_path = amxc_var_key(intf);
        regex pattern("WiFi.Radio.\\d+.");
        if (!regex_match(intf_path, pattern)) {
            continue;
        }

        WifiRadio radio;
        string freq = GET_CHAR(intf, "OperatingFrequencyBand") ? : "";
        radio.set_name(getRadioShortname(freq));
        radio.set_channel(GET_UINT8(intf, "Channel"));
        radio.set_bandwidth(GET_CHAR(intf, "OperatingChannelBandwidth") ? : "");
        radio.set_currentbandwidth(GET_CHAR(intf, "CurrentOperatingChannelBandwidth") ? : "");
        radio.set_autochannelenable(GET_BOOL(intf, "AutoChannelEnable") ? "TRUE" : "FALSE");
        radio.set_channelchangereason(GET_CHAR(intf, "ChannelBandwidthChangeReason") ? : "");
        radio.set_frequency(freq);
        radio.set_status(string(GET_CHAR(intf, "Status") ? : "") == "Up" ? "ON" : "OFF");
        /* Not found */
        // Don't set it for now
        // radio.set_cleareddfschannels();
        /* END */

        amxc_var_t ret;
        amxc_var_init(&ret);
        string stats_path = amxc_var_key(intf);
        stats_path.append("Stats.");
        int err = amxb_get(bus_ctx, stats_path.c_str(), 0, &ret, 5);
        if (err != 0) {
            SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
        }

        amxc_var_t *stats = NULL;
        amxc_var_for_each(item, amxc_var_get_first(&ret)) {
            string key = amxc_var_key(item);
            if (stats_path == key) {
                stats = item;
                break;
            }
        }
        if (!stats) {
            amxc_var_clean(&ret);
            continue;
        }

        radio.set_dfschannelchangeeventcounter(GET_UINT32(stats, "DFSChannelChangeEventCounter"));

        Event *event = event_list->add_event();
        event->set_kpiname(SOURCE);
        event->set_kpitype("Radio");
        fill_timestamp(event);
        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(radio);
        
        amxc_var_clean(&ret);
    }

    amxb_close_request(&req);

    return event_list;
}

/* END Radio */

/* RadioAirStats */

EventList *getRadioAirStatsEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return NULL;
    }

    amxb_request_t *req = amxb_async_call(bus_ctx, "WiFi.Radio.", "_get", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return NULL;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return NULL;
    }

    EventList *event_list = new EventList();

    amxc_var_for_each(intf, amxc_var_get_first(req->result)) {

        if (amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, intf))) {
            continue;
        }

        string intf_path = amxc_var_key(intf);
        regex pattern("WiFi.Radio.\\d+.");
        if (!regex_match(intf_path, pattern)) {
            continue;
        }

        WifiRadioAirStats radioAirStats;
        string freq = GET_CHAR(intf, "OperatingFrequencyBand") ? : "";
        radioAirStats.set_interface(getRadioShortname(freq));
        radioAirStats.set_channel(GET_UINT8(intf, "Channel"));

        amxb_request_t *req2 = amxb_async_call(bus_ctx, intf_path.c_str(), "getRadioAirStats", NULL, NULL, NULL);
        if (req2 == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to do async call");
            return NULL;
        }
        amxb_wait_for_request(req2, 10);

        if (amxc_var_is_null(req2->result)) {
            SAH_TRACEZ_ERROR(ME, "Async call request failed");
            amxb_close_request(&req2);
            return NULL;
        }

        radioAirStats.set_txop(100 - GETP_UINT32(req2->result, "0.Load") ? : 0);
        radioAirStats.set_load(GETP_UINT32(req2->result, "0.Load") ? : 0);
        radioAirStats.set_txtime(GETP_UINT32(req2->result, "0.TxTime") ? : 0);
        radioAirStats.set_rxtime(GETP_UINT32(req2->result, "0.RxTime") ? : 0);
        radioAirStats.set_inttime(GETP_UINT32(req2->result, "0.IntTime") ? : 0);
        radioAirStats.set_freetime(GETP_UINT32(req2->result, "0.FreeTime") ? : 0);
        radioAirStats.set_totaltime(GETP_UINT32(req2->result, "0.TotalTime") ? : 0);

        // Change the proto or do index
        // radioAirStats.set_longpreambleerror(GET_UINT32(req2->result, "LongPreambleErrorPercentage") ? : 0);
        // radioAirStats.set_shortpreambleerror(GET_UINT32(req2->result, "ShortPreambleErrorPercentage") ? : 0);
        // radioAirStats.set_badplcp(GET_INT32(req2->result, "BadPlcp") ? : 0);
        // radioAirStats.set_glitch(GET_INT32(req2->result, "Glitch") ? : 0);

        amxb_close_request(&req2);

        amxc_var_t ret;
        amxc_var_init(&ret);
        string stats_path = amxc_var_key(intf);
        stats_path.append("Stats.");
        int err = amxb_get(bus_ctx, stats_path.c_str(), 1, &ret, 5);
        if (err != 0) {
            SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
        }

        amxc_var_t *stats = NULL;
        amxc_var_for_each(item, amxc_var_get_first(&ret)) {
            string key = amxc_var_key(item);
            if (stats_path == key) {
                stats = item;
                break;
            }
        }
        if (!stats) {
            amxc_var_clean(&ret);
            continue;
        }

        radioAirStats.set_noise(GET_INT32(stats, "Noise"));

        Event *event = event_list->add_event();
        event->set_kpiname(SOURCE);
        event->set_kpitype("RadioAirStats");
        fill_timestamp(event);
        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(radioAirStats);

        amxc_var_clean(&ret);
    }

    amxb_close_request(&req);

    return event_list;
}

/* END RadioAirStats */

/* StationStats */

EventList *getStationStatsEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return NULL;
    }

    amxb_request_t *req = amxb_async_call(bus_ctx, "WiFi.AccessPoint.", "_get", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return NULL;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return NULL;
    }

    EventList *event_list = new EventList();

    amxc_var_for_each(ap, amxc_var_get_first(req->result)) {

        if (amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, ap))) {
            continue;
        }

        string ap_path = amxc_var_key(ap);
        regex pattern("WiFi.AccessPoint.\\d+.");
        if (!regex_match(ap_path, pattern)) {
            continue;
        }

        string status_ap = GET_CHAR(ap, "Status") ? : "";
        if (status_ap != "Enabled") {
            SAH_TRACEZ_INFO(ME, "AP is %s", status_ap.c_str());
            continue;
        }

        amxc_var_t rep;
        amxc_var_init(&rep);
        string stats_path = amxc_var_key(ap);
        stats_path.append("AssociatedDevice.");
        int err = amxb_get(bus_ctx, stats_path.c_str(), 1, &rep, 5);
        if (err != 0) {
            SAH_TRACEZ_ERROR(ME, "error amxb_get : %d", err);
        }

        amxc_var_for_each(device, amxc_var_get_first(&rep)) {
            string key = amxc_var_key(device);
            regex pattern(stats_path + "\\d+.");
            if (!regex_match(key, pattern)) {
                continue;
            }


            bool active = GET_BOOL(device, "Active");
            if (active == false) {
                SAH_TRACEZ_INFO(ME, "Device is inactive");
                continue;
            }

            WifiStationStats stationStats;
            stationStats.set_interface(getRadioShortname(getFreqFromRadioRef(string(GET_CHAR(ap, "RadioReference") ? : ""))));
            stationStats.set_flag("VAP");
            /* Not found */
            stationStats.set_vapmode("Undefined");
            stationStats.set_hotspottype("Undefined");
            /* END */
            stationStats.set_macaddress(GET_CHAR(device, "MACAddress") ? : "");
            stationStats.set_authenticationstate(GET_BOOL(device, "AuthenticationState"));
            stationStats.set_lastdatadownlinkrate(GET_UINT32(device, "LastDataDownlinkRate"));
            stationStats.set_lastdatauplinkrate(GET_UINT32(device, "LastDataUplinkRate"));
            stationStats.set_signalstrength(GET_INT32(device, "SignalStrength"));
            stationStats.set_active(active);
            stationStats.set_signalnoiseratio(GET_INT32(device, "SignalStrength") - GET_INT32(device, "Noise"));
            stationStats.set_noise(GET_INT32(device, "Noise"));
            stationStats.set_inactive(GET_UINT64(device, "Inactive"));
            stationStats.set_mode(GET_CHAR(device, "OperatingStandard") ? : "");
            stationStats.set_devicetype(GET_CHAR(device, "DeviceType") ? : "");
            stationStats.set_devicepriority(GET_INT32(device, "DevicePriority"));

            /* Stats */
            stationStats.set_rxpacketcount(GET_UINT64(device, "RxPacketCount"));
            stationStats.set_txpacketcount(GET_UINT64(device, "TxPacketCount"));
            stationStats.set_rxbytes(GET_UINT64(device, "RxBytes"));
            stationStats.set_txbytes(GET_UINT64(device, "TxBytes"));
            stationStats.set_rxretrans(GET_UINT64(device, "Rx_Retransmissions"));
            stationStats.set_txretrans(GET_UINT64(device, "Tx_Retransmissions"));
            stationStats.set_uplinkmcs(GET_UINT32(device, "UplinkMCS"));
            /* Not found */
            stationStats.set_uplinkis40mhz(0);
            /* END */
            stationStats.set_uplinkshortguard(GET_UINT32(device, "UplinkShortGuard"));
            stationStats.set_downlinkmcs(GET_UINT32(device, "DownlinkMCS"));
            /* Not found */
            stationStats.set_downlinkis40mhz(0);
            /* END */
            stationStats.set_downlinkshortguard(GET_UINT32(device, "DownlinkShortGuard"));
            stationStats.set_maxrxspatialstreamssupported(GET_UINT32(device, "MaxRxSpatialStreamsSupported"));
            stationStats.set_maxdownlinkratesupported(GET_UINT32(device, "MaxDownlinkRateSupported"));
            stationStats.set_maxdownlinkratereached(GET_UINT32(device, "MaxDownlinkRateReached"));
            stationStats.set_maxbandwidthsupported(GET_CHAR(device, "MaxBandwidthSupported") ? : "");
            stationStats.set_averagesignalstrength(GET_INT32(device, "AvgSignalStrength"));
            stationStats.set_maxsignalstrength(0);

            /* Not found */
            // stationStats.set_signalstrengthbychain(0);
            /* END */

            Event *event = event_list->add_event();
            event->set_kpiname(SOURCE);
            event->set_kpitype("StationStats");
            fill_timestamp(event);
            Any *event_data = event->mutable_event_data();
            event_data->PackFrom(stationStats);
        }

        amxc_var_clean(&rep);
    }

    amxb_close_request(&req);

    return event_list;
}

/* END StationStats */

/* Scan */

EventList* getScanEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return NULL;
    }

    amxb_request_t *req = amxb_async_call(bus_ctx, "WiFi.", "NeighboringWiFiDiagnostic", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return NULL;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return NULL;
    }

    string scan_status = GETP_CHAR(req->result, "0.Status") ? : "";
    if (scan_status != "Complete") {
        SAH_TRACEZ_INFO(ME, "Status: %s", scan_status);
        amxb_close_request(&req);
        return NULL;
    }

    EventList *scanList = new EventList();

    amxc_var_for_each(result, GETP_ARG(req->result, "0.Result")) {
        // amxc_var_dump(result, STDOUT_FILENO);
        Event *event = scanList->add_event();
        event->set_kpiname(SOURCE);
        event->set_kpitype("Scan");
        fill_timestamp(event);
        WifiScan scan;

        scan.set_scan(getRadioShortname(GET_CHAR(result, "OperatingFrequencyBand") ? : ""));
        scan.set_ssid(GET_CHAR(result, "SSID") ? : "");
        scan.set_bssid(GET_CHAR(result, "BSSID") ? : "");
        scan.set_signalnoiseratio(GET_INT32(result, "SignalStrength") - GET_INT32(result, "Noise"));
        scan.set_noise(GET_INT32(result, "Noise"));
        scan.set_signalstrength(GET_INT32(result, "SignalStrength"));
        scan.set_channel(GET_UINT32(result, "Channel"));
        
        /* Not found, I don't understand the diff with Channel */
        scan.set_centrechannel(GET_UINT32(result, "Channel"));
        /* END */
        string bandwidth = GET_CHAR(result, "OperatingChannelBandwidth") ? : "";
        bandwidth.erase(bandwidth.end() - 3, bandwidth.end());
        scan.set_bandwidth(atoi(bandwidth.c_str()));

        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(scan);
    }

    amxb_close_request(&req);

    return scanList;
}

/* END Scan */

/* ScanCount */

EventList* getScanCountEventList() {
    amxb_bus_ctx_t *bus_ctx = amxb_be_who_has("WiFi.");
    if (!bus_ctx) {
        SAH_TRACEZ_ERROR(ME, "Cannot retrieve bus ctx");
        return NULL;
    }

    amxb_request_t *req = amxb_async_call(bus_ctx, "WiFi.", "NeighboringWiFiDiagnostic", NULL, NULL, NULL);
    if (req == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to do async call");
        return NULL;
    }
    amxb_wait_for_request(req, 10);

    if (amxc_var_is_null(req->result)) {
        SAH_TRACEZ_ERROR(ME, "Async call request failed");
        amxb_close_request(&req);
        return NULL;
    }

    string scan_status = GETP_CHAR(req->result, "0.Status") ? : "";
    if (scan_status != "Complete") {
        SAH_TRACEZ_INFO(ME, "Status: %s", scan_status);
        amxb_close_request(&req);
        return NULL;
    }

    amxc_var_t ssid_count_table;
    amxc_var_init(&ssid_count_table);
    amxc_var_set_type(&ssid_count_table, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(result, GETP_ARG(req->result, "0.Result")) {
        string freq = GET_CHAR(result, "OperatingFrequencyBand") ? : "";
        amxc_var_t *it = amxc_var_get_key(&ssid_count_table, freq.c_str(), AMXC_VAR_FLAG_DEFAULT);
        if (it == NULL) {
            it = amxc_var_add_key(uint32_t, &ssid_count_table, freq.c_str(), 0);
        }
        uint32_t count = GET_UINT32(it, NULL) + 1;
        amxc_var_set(uint32_t, it, count);
    }

    EventList *scanCountList = new EventList();

    amxc_var_for_each(entry, &ssid_count_table) {
        Event *event = scanCountList->add_event();
        event->set_kpiname(SOURCE);
        event->set_kpitype("ScanCount");
        fill_timestamp(event);
        WifiScanCount scanCount;

        string freq = amxc_var_key(entry);
        scanCount.set_interface(getRadioShortname(freq));
        scanCount.set_ssidcount(GET_UINT32(entry, NULL));

        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(scanCount);
    }

    amxc_var_clean(&ssid_count_table);
    amxb_close_request(&req);

    return scanCountList;
}

/* END ScanCount */

/* Init */

void wifi_init() {
    auto client = init_grpc_client();

    ProbeAmxClientModule interface_stats(&client, &getInterfaceStatsEventList, 600, true);
    ProbeAmxClientModule radio(&client, &getRadioEventList, 3600, true);
    ProbeAmxClientModule radio_air_stats(&client, &getRadioAirStatsEventList, 180, true);
    ProbeAmxClientModule scan(&client, &getScanEventList, 3600, true);
    ProbeAmxClientModule scan_count(&client, &getScanCountEventList, 3600, true);
    ProbeAmxClientModule station_stats(&client, &getStationStatsEventList, 600, true);

    amxrt_el_start();
}

int wifi() {
    RestoreConfiguration(wifi_configuration, DEFAULT_FILE, PERSIST_FILE);
    InitSahTrace(wifi_configuration.sahtrace_config());
    init_amx("wifi-module", {"WiFi.", "Device.WiFi."});
    wifi_init();

    /* Cleanup */
    CleanupSahTrace();
    BackupConfiguration(wifi_configuration, PERSIST_FILE);
    amxrt_stop();
    amxrt_delete();

    return 0;
}

/* END Init */
